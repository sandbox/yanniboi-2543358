/**
 * @file
 * Handles AJAX submission and response in Views UI.
 */

(function ($, Drupal, drupalSettings) {

  "use strict";

  /**
   * Sync preview display.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.angularBlockAppInit = {
    attach: function (context) {
      var module = angular.module('drupalApp', []);

      module.config(function($interpolateProvider){
        console.log('config');
        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
      });

      module.controller("BlockCtrl", function($scope) {
        console.log('Controller');
        $scope.name = "Developer";

        $scope.add = function () {
          $scope.name += ' Test';
        }
      });


    }
  };

})(jQuery, Drupal, drupalSettings);
