<?php

/**
 * @file
 * Contains \Drupal\angular_block\Plugin\Block\AngularBlockBlock.
 */

namespace Drupal\angular_block\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Search form' block.
 *
 * @Block(
 *   id = "angular_block_block",
 *   admin_label = @Translation("Angular Block"),
 *   category = @Translation("Angular")
 * )
 */
class AngularBlockBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'access content');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $content = array(
      '#markup' => '',
      '#theme' => 'angular_block',
      '#attached' => array(
        'library' => array(
          'angular_block/angular-block'
        ),
      ),
    );

    return $content;
  }

}